require('dotenv').config();
require('./app/prototypes');
require('./app/services');

const {Client, Collection} = require('discord.js');
const config = require('./configs/app');

const client = new Client(config.discord);
client.commands = new Collection();
client.cooldowns = new Collection();

require('./commands/help').register(client);
require('./commands/uptime').register(client);
require('./commands/ticket-close').register(client);
require('./commands/ticket-add-user').register(client);
require('./commands/ticket-rem-user').register(client);
require('./commands/ticket-new-channel').register(client);
require('./commands/vote-bump-server').register(client);

client.once('ready', function () {
    console.log('Your discord bot is ready.');
});
client.on('message', function (message) {
    if (message.author.bot) return;
    handleCommand(message);
});
client.on('messageReactionAdd', async function (reaction, user) {
    if (user.bot) return;
    if (reaction.message.partial) await reaction.message.fetch();
    require('./app/ticket-reactions')(reaction, user);
});
client.on('messageDelete', function (message) {
    services.ticketPersist.removeCache('message_id', message.id);
});
client.on('channelDelete', function (channel) {
    services.ticketPersist.removeCache('channel_id', channel.id);
});
client.on('error', console.error);
client.login(config.token);

process.on('unhandledRejection', console.trace);

function handleCommand(message) {
    const content = message.content.trim();
    if (content.startsWith(config.prefix) == false) return;
    const args = content.replace(config.prefix, '').split(' ');
    const name = args.shift().toLowerCase();
    const command = client.commands.find(function (item) {
        return item.name == name || item.aliases.includes(name);
    });
    if (command == undefined) return;
    command.process(message, args);
}