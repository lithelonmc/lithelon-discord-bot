module.exports = {
    username: process.env.VOTE_USERNAME,
    password: process.env.VOTE_PASSWORD,
    puppeteer: {
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    },
    selectors: {
        username: 'input[id=login_username]',
        password: 'input[id=login_password]'
    }
};