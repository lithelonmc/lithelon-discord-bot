const {MessageEmbed} = require('discord.js');
const {startCase} = require('lodash');
const BaseCommand = require('./base');
const persist = services.ticketPersist;

module.exports = new BaseCommand({
    name: 'newticketchannel',
    description: 'Setup this channel to start accepting tickets.',
    execute(message) {
        const {channel, member} = message;
        if (member.permissions.has('MANAGE_CHANNELS') == false) return;
        const existing = persist.findCache('channel_id', channel.id);
        if (existing) return message.reply('Ticket system is already setup for this channel.');
        const embed = new MessageEmbed({
            title: startCase(channel.name),
            description: 'React to this message with 📨 to start new ticket.'
        });
        channel.send(embed).then(function (item) {
            item.react('📨');
            persist.addCache({
                cache_type: 0,
                guild_id: channel.guild.id,
                channel_id: channel.id,
                message_id: item.id
            });
        });
        message.delete();
    }
});