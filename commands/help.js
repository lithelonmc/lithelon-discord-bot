const BaseCommand = require('./base');

module.exports = new BaseCommand({
    name: 'help',
    description: 'Shows the available bot commands.',
    execute(message) {
        const data = new Array;
        const {client, author, channel} = message;
        data.push(`${author}, heres a list of my commands.`);
        client.commands.forEach(function (item) {
            data.push(`**${item.usageDisplay}**`);
        });
        return channel.send(data, {
            split: true
        }).catch(console.error);
    }
});