const BaseCommand = require('./base');
const TicketUtils = require('../app/ticket-utils');
const persist = services.ticketPersist;

module.exports = new BaseCommand({
    name: 'ticketrem',
    description: 'Removes a user from the ticket.',
    usage: '<user>',
    args: 1,
    execute(message) {
        const {channel, author, mentions} = message;
        const cache = persist.findCache('channel_id', channel.id);
        if (TicketUtils.isClaimer(channel, author, cache) == false) return;
        const member = mentions.members.first();
        if (member == null) return message.reply('We could not find that user.');
        channel.permissionOverwrites.get(member.id).delete().then(function () {
            channel.send(`${member} has been removed from the ticket.`);
        });
    }
});