const axios = require('axios');
const config = require('../configs/app');

class Paste {
    static paste(name, contents) {
        return axios.post('https://api.paste.ee/v1/pastes', {
            description: 'Discord Bot Paste',
            sections: [
                {
                    name: name,
                    syntax: 'text',
                    contents: contents
                }
            ]
        }, Paste.axiosOptions);
    }

    static get axiosOptions() {
        return {
            headers: {
                'X-Auth-Token': config.paste_key
            }
        };
    }
}

module.exports = Paste;