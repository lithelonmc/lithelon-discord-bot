const {get} = require('lodash');

class Utils {
    static find(items, name, value) {
        return items.find(function (item) {
            return get(item, name) == value;
        });
    }
}

module.exports = Utils;