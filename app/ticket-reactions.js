const config = require('../configs/tickets');
const persist = services.ticketPersist;

module.exports = function (reaction, user) {
    const {message, emoji} = reaction;
    const cache = persist.findCache('channel_id', message.channel.id);
    if (cache == undefined) return;
    if (cache.cache_type == 0) {
        if (emoji.name == '📨') {
            createTicketChannel(message, user);
        }
    } else if (cache.cache_type == 1) {
        if (message.channel.parent.id == config.parent_category) {
            switch (emoji.name) {
                case '🙋':
                    claimTicket(message, user, cache);
                    break;
                case '🪁':
                    releaseTicket(message, user, cache);
                    break;
            }
        }
    }
    reaction.users.remove(user);
};

function releaseTicket(message, user, cache) {
    if (cache.claim_user_id == user.id) {
        const channel = message.channel;
        config.roles.forEach(function (role) {
            channel.createOverwrite(role, {
                READ_MESSAGE_HISTORY: true,
                VIEW_CHANNEL: true,
                SEND_MESSAGES: false
            });
        });
        channel.permissionOverwrites.get(cache.claim_user_id).delete();
        channel.send(`${user} has released the ticket for claiming.`);
        cache.claim_user_id = null;
        persist.save();
    }
}

function canManageTicket(message, user) {
    return message.guild.members.fetch(user.id).then(function (member) {
        return config.roles.some(function (item) {
            return member.roles.cache.has(item);
        });
    });
}

function claimTicket(message, user, cache) {
    canManageTicket(message, user).then(function (allowed) {
        if (allowed == false) return;
        if (cache.claim_user_id == null) {
            const channel = message.channel;
            config.roles.forEach(function (role) {
                channel.permissionOverwrites.get(role).delete();
            });
            channel.createOverwrite(user, {
                VIEW_CHANNEL: true,
                SEND_MESSAGES: true
            });
            channel.send(`${user} has claimed this ticket and will be helping you.`);
            cache.claim_user_id = user.id;
            persist.save();
        }
    });
}

function createTicketChannel(message, user) {
    const {channel, guild} = message;
    const overwrites = config.roles.map(function (role) {
        return {
            id: role,
            allow: 1024,
            deny: 2048
        };
    });
    overwrites.push({
        id: user.id,
        allow: 3072
    });
    overwrites.push({
        id: guild.id,
        deny: 3072
    });
    guild.channels.create(`${channel.name}-${user.username}`, {
        parent: config.parent_category,
        permissionOverwrites: overwrites
    }).then(function (channel) {
        channel.send('Please explain with as much detail why you opened this ticket.').then(function (item) {
            item.react('🙋');
            item.react('🪁');
            persist.addCache({
                cache_type: 1,
                claim_user_id: null,
                user_create_id: user.id,
                user_create_name: user.username,
                guild_id: channel.guild.id,
                channel_id: channel.id,
                message_id: item.id
            });
        });
    }).catch(console.error);
}